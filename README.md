## Tested several approach to GitLab CI/CD pipeline optimization (issue with GitLab caching)

# Used configuration with a lot of npm packages (> 2100) and simple next.js application

# Tested approaches:

- branch: used-preinstalled-docker-image - Build a docker image with installed packages and use it in other jobs

- branch: used-artifacts-between-stages - Used artifact with node_modules folder between stages

- branch: used-artifacts-between-stages-cache-between-pipelines - Used artifact with node_modules folder between stages and cache between pipelines

- branch: used-artifacts-between-stages-cache-between-pipelines-with-yarn2 - Used artifact with node_modules folder between stages and cache between pipelines

# Info

- [GitLab issue: Caching is very slow for Node.JS projects](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1797)
- [Caching is very slow for Node.JS projects](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1797#note_427509128)
- [Speed up NPM/Yarn install in Gitlab](https://medium.com/disdj/speed-up-npm-yarn-install-in-gitlab-1434437f9857)
- [How to Speed Up Your GitLab CI Pipelines for Node Apps by 40%](https://www.addthis.com/blog/2019/05/06/how-to-speed-up-your-gitlab-ci-pipelines-for-node-apps-by-40/#.X47Q89AzaUk)
- [Add fastzip archiver/extractor](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2210)
